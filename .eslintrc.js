module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  extends: ['airbnb', 'prettier'],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
    Generator: true,
    Iterable: true,
    $ReadOnlyArray: true,
  },
  parser: "babel-eslint",
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: [
    'react',
    'prettier'
  ],
  rules: {
    'semi': 0,
    'react/jsx-filename-extension': [1, { "extensions": [".js", ".jsx"] }],
    'comma-dangle': ["error", "never"],
    'react/prop-types': 0,
    'react/destructuring-assignment': 0,
    'import/prefer-default-export': 'off',
    'noUselessIndex': false,
    'no-useless-path-segments': false,
    'prettier/prettier': ['error', {'singleQuote': true, 'parser': 'flow', 'semi': false }],
    'import/no-useless-path-segments': 'off',
  },
};
