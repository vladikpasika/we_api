import mongoose from 'mongoose'

class mongoseSingletone {
  constructor() {
    this.instance = null
  }

  static getInstance() {
    if (!this.instance) {
      mongoose.connect('mongodb://localhost:27017/WE', {
        useNewUrlParser: true
      })
      mongoose.connection
        .once('open', () => {
          console.log('Подключились к базе данных')
        })
        .on('error', err => {
          console.error('Connection error:', err)
        })
      this.instance = mongoose
    }
    return this.instance
  }
}

export default mongoseSingletone
