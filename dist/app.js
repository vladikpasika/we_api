"use strict";

var _koa = _interopRequireDefault(require("koa"));

var _koaBodyparser = _interopRequireDefault(require("koa-bodyparser"));

var _koaDecodedQuerystring = _interopRequireDefault(require("koa-decoded-querystring"));

var _koaStatic = _interopRequireDefault(require("koa-static"));

var _constants = require("../constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

require('@babel/polyfill');

require('@babel/register');

var app = new _koa["default"]();
app.use((0, _koaBodyparser["default"])());
app.use((0, _koaDecodedQuerystring["default"])());
app.use((0, _koaStatic["default"])('../public'));
app.listen(_constants.PORT);
console.log(_constants.PORT);