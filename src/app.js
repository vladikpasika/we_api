/** @flow */
import Koa from 'koa'
import bodyParser from 'koa-bodyparser'
import koaDecodedQuerystring from 'koa-decoded-querystring'
import serve from 'koa-static'
import session from 'koa-session'
import passport from 'koa-passport'
import MongoStore from 'koa-session-mongoose'
import cookieParser from 'koa-cookie'
import convert from 'koa-convert'
import errorHandler from 'koa-better-error-handler'
import koa404Handler from 'koa-404-handler'

import mongooseConnection from '../connections/mongoDBConnection'
import configureRouter from '../api/controllers'

import { PORT } from '../constants'

const router = configureRouter()
const mongooseInstance = mongooseConnection.getInstance()
const app = new Koa()
app.context.onerror = errorHandler
// specify that this is our api
app.context.api = true

// use koa-404-handler
app.use(koa404Handler)

app.use(bodyParser())
app.use(cookieParser())
app.use(koaDecodedQuerystring())
app.use(serve('../public'))

app.keys = ['some secret key']
app.use(
  session(
    {
      store: new MongoStore({
        collection: 'sessions',
        connection: mongooseInstance
      })
    },
    app
  )
)

app.use(passport.initialize())
app.use(passport.session())

app.use(router.routes()).use(router.allowedMethods())
app.listen(PORT)

console.log(`${PORT}`)
