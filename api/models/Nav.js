import { Schema, model } from 'mongoose'
import autoincrement from 'simple-mongoose-autoincrement'

const navSchema = new Schema({
  title: String,
  link: String
})

navSchema.plugin(autoincrement, { field: 'id' })
const NavModel = model('nav', navSchema)

export async function getAllNav() {
  const allNav = await NavModel.find()
  return allNav
}
