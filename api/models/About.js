import { Schema, model } from 'mongoose'
import autoincrement from 'simple-mongoose-autoincrement'

const aboutSchema = new Schema({
  title: String,
  description: String,
  text: String
})

aboutSchema.plugin(autoincrement, { field: 'id' })

export const AboutModel = model('about', aboutSchema)

export async function createAbout(about) {
  // const newAbout = await AboutModel.save(about)
  // return newAbout
}

export async function findAbout(query) {
  const findedAbout = await AboutModel.find(query)
  return findedAbout
}

export async function getAllAbout() {
  const allAbout = await AboutModel.find()
  return allAbout
}

export async function removeAbout(query) {
  const removed = await AboutModel.remove(query)
  return removed
}
