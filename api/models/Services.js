import { Schema, model } from 'mongoose'
import autoincrement from 'simple-mongoose-autoincrement'

const servicesSchema = new Schema({
  title: String,
  description: String,
  price: Number,
  currency: String,
  img: String
})

servicesSchema.plugin(autoincrement, { field: 'id' })

export const serviceModel = model('services', servicesSchema)

export async function createService(service) {
//   const createdService = await serviceModel.save(service)
//   return createdService
}

export async function findServices(query) {
  const findedServices = await serviceModel.find(query)
  return findedServices
}

export async function getAllServices() {
  const allServices = await serviceModel.find()
  return allServices
}

export async function removeService(query) {
  const removedService = serviceModel.remove(query)
  return removedService
}
