import { Schema, model } from 'mongoose'
import autoincrement from 'simple-mongoose-autoincrement'

const reviewsSchema = new Schema({
  name: String,
  reviewModel: String,
  src: String,
  src_type: String,
  social_link: String,
  type_social: String
})

reviewsSchema.plugin(autoincrement, { field: 'id' })
export const ReviewModel = model('reviews', reviewsSchema)

export async function createReview(review) {
  // const createdRewiew = await ReviewModel.save(review)
  // return createdRewiew
}

export async function findReviews(query = {}) {
  const findedReview = await ReviewModel.find(query)
  return findedReview
}

export async function getAllReviews() {
  const allReviews = await ReviewModel.find()
  return allReviews
}

export async function removeReview(query) {
  const removedReview = await ReviewModel.remove(query)
  return removedReview
}
