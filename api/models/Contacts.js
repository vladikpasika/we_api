import { Schema, model } from 'mongoose'
import autoincrement from 'simple-mongoose-autoincrement'

const contactsSchema = new Schema({
  firm_name: String,
  description: String,
  phones: String,
  skype: String,
  viber: String,
  e_mail: String
})

contactsSchema.plugin(autoincrement, { field: 'id' })

export const ContactModel = model('contacts', contactsSchema)

export async function getAllContacts() {
  const contacts = await ContactModel.find()
  return contacts
}
