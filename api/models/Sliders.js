import { Schema, model } from 'mongoose'
import autoincrement from 'simple-mongoose-autoincrement'

const SliderSchema = new Schema({
  title: String,
  description: String,
  background: String
})
SliderSchema.plugin(autoincrement, { field: 'id' })

export const SliderModel = model('mainSlider', SliderSchema)

export async function getAllSliders() {
  const allSliders = await SliderModel.find()
  return allSliders
}
