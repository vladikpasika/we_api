import mongoose, { Schema } from 'mongoose'
import bcrypt from 'bcrypt'

const saltRounds = 10

const userSchema = new Schema({
  username: {
    type: String,
    index: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  permissions: {
    type: String,
    required: false
  }
})

export const User = mongoose.model('users', userSchema)

export async function createUser(newUser) {
  const modelOfUser = new User({ ...newUser })
  const hash = await bcrypt
    .hash(modelOfUser.password, saltRounds)
    .catch(error => console.error(error))
  // eslint-disable-next-line no-param-reassign
  modelOfUser.password = hash
  const result = await modelOfUser.save().catch(error => console.error(error))
  return result
}

export async function getUserByEmail(email) {
  const user = await User.findOne({ email: email.toLowerCase() })
  return user
}

export async function comparePasswords(email, password) {
  const findedUser = await User.findOne({ email })
  if (!findedUser) {
    throw new Error('User not found')
  }
  const passwordIsPass = await bcrypt.compare(password, findedUser.password)
  return passwordIsPass && findedUser
}
