import { Schema, model } from 'mongoose'
import autoincrement from 'simple-mongoose-autoincrement'

const clientsSchema = new Schema({
  big_photo: String,
  small_photo: String
})

clientsSchema.plugin(autoincrement, { field: 'id' })

export const clientModel = model('clients', clientsSchema)

export async function createClient(newClient) {
  // /
}
export async function findClientsPhotos(query) {
  const findedClientsPhotos = await clientModel.find(query)
  return findedClientsPhotos
}
export async function getAllClientsPhotos() {
  const allClientsPhotos = await clientModel.find()
  return allClientsPhotos
}
export async function removeClientsPhotos(query) {
  const removedClientsPhotos = await clientModel.remove(query)
  return removedClientsPhotos
}
