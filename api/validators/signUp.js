import Joi from 'joi'
import validate from 'koa-joi-validate'

export const signUpValidator = validate({
  body: {
    username: Joi.string().required(),
    password: Joi.string().required(),
    email: Joi.string().email()
  }
})
