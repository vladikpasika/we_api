import Joi from 'joi'
import validate from 'koa-joi-validate'

export const writeUsValidator = validate({
  body: {
    name: Joi.string().required(),
    phone: Joi.string().required()
  }
})
