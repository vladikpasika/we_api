import Joi from 'joi'
import validate from 'koa-joi-validate'

export const signInValidator = validate({
  body: {
    password: Joi.string().required(),
    email: Joi.string().email()
  }
})
