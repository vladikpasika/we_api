import passport from 'koa-passport'
import mongoConnection from '../../connections/mongoDBConnection'
import { getUserByEmail, comparePasswords, User } from '../models'
import { Strategy } from 'passport-local'

passport.serializeUser(async (user, done) => (done(null, user.id)));

  passport.deserializeUser(async (id, done) => {
    try {
      const user = await User.findOne({_id: id})
      if (!user) {
        return done(new Error('user not found'))
      }
      done(null, user)
    } catch (e) {
      done(e)
    }
  });
  

 passport.use(
  new Strategy(
    { usernameField: 'email', passwordField: 'password' }, // rename fields
    async function(username, password, done) {
      const emailInLowerCase = username.toLowerCase() // username means email
        const matchUser = await comparePasswords(emailInLowerCase, password)
        if(!matchUser) {
          return done(null, matchUser, { message: 'Wrong password'})
        }
        return done(null, matchUser)
    }
  )
)

export default passport
