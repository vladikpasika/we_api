import axiosInstance from '../../../axiosInstance'
import { API_SERVER } from '../../../../constants'

export default async ctx => {
  const axios = axiosInstance.getInstance({
    baseURL: API_SERVER
  })
  const response = await axios.get('/users/xss').catch(error => {
    console.log(error)
  })
  // console.log(response.data)
  return (ctx.body = response.data)
}
