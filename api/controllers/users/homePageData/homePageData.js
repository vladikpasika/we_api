import {
  getAllReviews,
  getAllAbout,
  getAllServices,
  getAllClientsPhotos,
  getAllContacts,
  getAllNav,
  getAllSliders
} from '../../../models'

export default async function homePageData(ctx, next) {
  const [
    reviews,
    about,
    services,
    clientsPhotos,
    contacts,
    nav,
    sliders
  ] = await Promise.all([
    getAllReviews(),
    getAllAbout(),
    getAllServices(),
    getAllClientsPhotos(),
    getAllContacts(),
    getAllNav(),
    getAllSliders()
  ])
  ctx.body = { reviews, about, services, clientsPhotos, contacts, nav, sliders }
  return next
}
