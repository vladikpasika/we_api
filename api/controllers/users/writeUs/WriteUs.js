import { NodeMailer } from '../../../../config'

export default async function WriteUs(ctx, next) {
  try {
    const result = await NodeMailer(ctx.request.body)
    ctx.body = result
    return ctx.body
  } catch (error) {
    return next
  }
}
