import Boom from 'boom'

import { getUserByEmail, createUser } from '../../../models'

export default async function(ctx, next) {
    const { body: { email, password, username } } = ctx.request
    const findedUser = await getUserByEmail(email)
    if (findedUser) {
        ctx.throw(Boom.conflict('User with this email alredy exist'))
    }
    const newUser = await createUser({email, password, username})
        .catch(error => (Boom.badImplementation('The error occurred while adding new User to DB', error)))
        ctx.body = newUser
        ctx.status = 201
        return ctx.body
}

