import Router from 'koa-router'

import userList from './userList'
import signUp from './sign-up'
import signIn from './sign-in'
import homePageData from './homePageData'
import writeUs from './writeUs'
import {
  signUpValidator,
  signInValidator,
  writeUsValidator
} from '../../validators'

export default function users() {
  const router = new Router()
  router.get('/users/xss', async (ctx, next) => {
    const list = await userList(ctx, next)
    return list
  })

  router.post('/sign-up', signUpValidator, async (ctx, next) => {
    const signUpUser = await signUp(ctx, next)
    return signUpUser
  })

  router.post('/sign-in', signInValidator, async (ctx, next) => {
    const signInUser = await signIn(ctx, next)
    return signInUser
  })

  router.get('/homepage', async (ctx, next) => {
    const homePage = await homePageData(ctx, next)
    return homePage
  })
  router.post('/write-us', writeUsValidator , async (ctx, next) => {
    const weiteUs = await writeUs(ctx, next)
    return weiteUs
  })

  router.get('/auth-status', async ctx => {
    if (ctx.isAuthenticated()) {
      const { username, permissions, email } = ctx.state.user
      return (ctx.body = {
        username,
        permissions,
        email,
        isAuth: true
      })
    }
    return (ctx.body = {
      userName: '',
      permissions: '',
      email: '',
      isAuth: false
    })
  })

  return router
}
