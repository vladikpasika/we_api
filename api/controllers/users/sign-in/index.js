import Boom from 'boom'

import passport from '../../../auth'
import { getUserByEmail } from '../../../models'

export default async function(ctx, next) {
  const {
    body: { email }
  } = ctx.request

  const emailInLowercase = email.toLowerCase()
  const findedUser = await getUserByEmail(emailInLowercase)

  if (!findedUser) {
    return ctx.throw(Boom.notFound('User with this email not found'))
  }
  const authResponse = await passport.authenticate(
    'local',
    async (err, user, msg) => {
      if (err) {
        return next(err)
      }

      if (!user) {
        return msg && ctx.throw(Boom.unauthorized(msg.message))
      }

      ctx.logIn(user, err => {
        if (err) {
          return (ctx.body = err)
        }
        const { username, permissions, email } = ctx.state.user
        return (ctx.body = {
          username,
          permissions,
          email,
          isAuth: true
        })
      })
    }
  )(ctx, next)

  return authResponse
}
