import Router from 'koa-router'

import users from './users'

export default function configureRouter() {
  const router = new Router({ prefix: '/api' })
  const usersRouter = users()
  router.use(usersRouter.routes()).use(usersRouter.allowedMethods())
  return router
}
