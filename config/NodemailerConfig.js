import nodemailer from 'nodemailer'

export default async function NodemailerConfig(emailBody) {
  const config = await import('../secret/nodemailer.json')
  const transporter = nodemailer.createTransport({
    service: 'gmail',
    secure: false,
    port: 25,
    auth: {
      user: config.email,
      pass: config.pass
    },
    tls: {
      rejectUnauthorized: false
    }
  })
  const sendedEmail = await transporter.sendMail({
    from: 'wladikpasika@gmail.com',
    to: 'world2016_emig@ukr.net, wladikpasika@gmail.com',
    subject: 'Заявка с сайта',
    text: `Телефон/Email: ${emailBody.phone}; Имя: ${emailBody.name}`
  })
  return sendedEmail
}
